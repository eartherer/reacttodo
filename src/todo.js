import React from 'react';
import ITEMLIST from './itemlist'
class TODO extends React.Component{

    constructor(){
        super()
        this.state = {
            input : '',
            items : [],
            isComplete : []
        }
        this.addItem = this.addItem.bind(this);
        this.inputChanged = this.inputChanged.bind(this);
        this.markComplete = this.markComplete.bind(this);
    }

    markComplete(index){
        console.log("Mark" + index);
        var tmpIsComplete = this.state.isComplete;
        tmpIsComplete[index] = !tmpIsComplete[index];
        this.setState({
            isComplete : tmpIsComplete
        });
    }

    inputChanged(e){
        this.setState({
            input : e.target.value
        });
    }

    addItem(e){
        console.log("Called AddItem");
        var newItem = this.state.input;
        var tmp = this.state.items.concat(newItem);
        var tmpIsComplete = this.state.isComplete.concat(false);

        console.log("New Item : "+newItem);
        this.setState({
            input : '',
            items : tmp,
            isComplete : tmpIsComplete
        });
    }

    render(){
        return (
            <div>
                <input type="text" onChange={this.inputChanged} value={this.state.input}/><button onClick={this.addItem}>Add</button>
                <br/>
                {this.state.isComplete.filter(x => x===false).length} remaining out of {this.state.isComplete.length} tasks
                {
                    <ul>
                        {
                            this.state.items.map((value, index) => {                                
                                return <ITEMLIST key={index} value={value} isComplete={this.state.isComplete[index]} callback={this.markComplete} dataIndex={index}/>
                            })
                        }
                    </ul>
                }
            </div>
        );
    }
}

export default TODO