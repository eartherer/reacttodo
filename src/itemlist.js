import React from 'react';

class ITEMLIST extends React.Component{

    constructor(){
        super()
        this.clicked = this.clicked.bind(this);
    }

    clicked(e){
        this.props.callback(this.props.dataIndex);
    }

    render(){
        if (this.props.isComplete) {
            return (
                <li><h3 onClick={this.clicked} style={{ textDecorationLine: 'line-through' }}>
                    {this.props.value}
                </h3></li>
            ); 
        }
        return (
            <li><h3 onClick={this.clicked}>
                {this.props.value}
            </h3></li>
        );
    }
}

export default ITEMLIST;